import sys
import os
########################################################
#   input file is parsed into localization dictionary  #
########################################################
#       localization = {                               #
#           "english": {                               #
#               "languages": {[                        #
#                   "en-US": "English",                #
#                   "fr-FR": "French"                  #
#               ]},                                    #
#               "tabBar": {[                           #
#                   "myLibrary": "My Library",         #
#                   ...                                #
#               ]}                                     #
#           },                                         #
#           "french": {...}                            #
#       }                                              #
########################################################

extra_keys = {"newSharedStory": 1}

def to_camel_case(snake_str):
    components = snake_str.split('_')
    # We capitalize the first letter of each component except the first one
    # with the 'title' method and join them together.
    return components[0] + ''.join(x.title() for x in components[1:])

#sections are added in files as comments
def addSections(localization, section_title):
    for language in localization.keys():
        localization[language][section_title] = []


def createLocalizations(path_to_file):
    file = open(path_to_file, "r")
    first_line = file.readline()
    columns = first_line.split("\t")

    if len(columns) < 2:
        print("File should have at least two columns (key lang1)")
        sys.exit(1)

    languages = []
    # replace "-1" with empty if COMMENTS header is removed
    for item in columns[1:-2]:
        language = item.strip()
        if language != '':
            languages.append(language)

    localization = {}
    for language in languages:
        localization[language] = {}

    currentSection = ""
    sections = []
    for line in file:
        columns = []
        for item in line.split("\t"):
            column = item.strip()
            if column != '':
                columns.append(column)

        if len(columns) == 1:
            currentSection = columns[0].strip()
            addSections(localization, currentSection)
            sections.append(currentSection) #just to keep order from the sheet
        elif len(columns) >= 2:
            key = columns[0]
            if currentSection == "LANGUAGES":
                key = key.replace("_", "-")
            if key in extra_keys:
                continue
            values = columns[1:]
            if len(values) < len(languages):
                print("Missing some translations for: " + key)
                print("Have: ")
                print(values)
                sys.exit(1)
            for index, language in enumerate(languages):
                value = values[index].replace("%s", "%@").replace("$url", "")
                localization[language][currentSection].append({to_camel_case(key): value})

    createLocalizationFiles(localization, sections, path_to_file)


def createLocalizationFiles(localizations, sections, path_to_file):
    folder = path_to_file.split("/")[1:-1]
    if len(folder) == 0:
        folder = os.getcwd()
    else:
        folder = "/" + "/".join(folder)
    print("Files saved to: " + folder)
    for language in localizations.keys():
        out = open(folder + "/Localizable.strings (" + language + ")", "w+")
        for section in sections:
            values = localizations[language][section]
            out.write("\n\n//MARK: " + section + "\n")
            for pair in values:
                key = pair.keys()[0]
                value = pair[key]
                out.write("\"" + key + "\" = \"" + value + "\";\n")
        out.close()

def main(argv):
    if len(argv) == 0:
        print("localizations.py <PathToFile>")
        sys.exit(1)
    elif argv[0][-4:] != ".tsv":
        print("File must be in .tsv format!-" + argv[0][-4:])
        sys.exit(1)

    path_to_file = argv[0]
    createLocalizations(path_to_file)


if __name__ == "__main__":
    main(sys.argv[1:])
